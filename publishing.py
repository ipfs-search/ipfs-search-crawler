#!/usr/bin/env python3
import requests, time, os, json
import Crawler.ipfs as ipfs

publishing_interval = int(os.environ["PUBLISHING_INTERVAL"])


def run():
    inverted_index = None
    while True:
        try:
            print("Inverted Index downloading")
            new_index   = requests.get("http://0.0.0.0:4002").json()
            if new_index != inverted_index:
                print("Publishing Index")
                result = ipfs.add_and_publish("INDEX", json.dumps(new_index))
                if result:
                    print(f"Published Index to {result}")
                else:
                    print("Publishing failed")
                inverted_index  = new_index
            else:
                print("Published  version is up-to-date")

        except requests.exceptions.ConnectionError:
            print("IPFS or Crawler currently unavailable")
        except Exception as e:
            raise e
        finally:
            time.sleep(60 * publishing_interval)

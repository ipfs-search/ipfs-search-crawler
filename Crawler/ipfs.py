import requests
import os

API_URL = os.environ["API_URL"]

def add(file):
    """
    Add a file to IPFS.

    Parameters:
    file, a file like object that is uploaded.

    Returns:
    The CID of the file to be used by cat.
    """
    return requests.post( f"{API_URL}/v0/add?cid-version=1"
                        , files = { "file" : file }
                        ).json()["Hash"]

def add_string(filename : str, content : str):
    """
    Add two strings as file name and content.

    Parameters:
    filename : str, the Filename of the file.
    content : str, the content of the file.

    Returns:
    The CID of the file to be used by cat for example.
    """
    return requests.post( f"{API_URL}/v0/add?cid-version=1"
                        , files = { "file" : (filename, content) }
                        ).json()["Hash"]

def cat(cid : str):
    """
    Get the file content of a cid.

    Parameters:
    cid : str, the CID of the file to fetch.

    Returns:
    The content of the File.
    """
    response    = requests.post( f"{API_URL}/v0/cat?arg={cid}")
    if response.ok:
        return response.text
    else:
        return False

def publish_cid(cid : str):
    response    = requests.post( f"{API_URL}/v0/name/publish?arg={cid}")

    if response.ok:
        return response.json()["Name"]
    else:
        return False

def add_and_publish(filename, content):
    cid = add_string(filename, content)
    return publish_cid(cid)

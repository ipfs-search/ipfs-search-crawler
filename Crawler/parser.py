#!/usr/bin/env python3
from html.parser import HTMLParser

class KeywordParser(HTMLParser):
    """docstring for KeywordParser."""

    def __init__(self):
        super(KeywordParser, self).__init__()
        self.keywords = []

    def handle_starttag(self, tag, attrs):
        if tag == "meta" and ('name', 'keywords') in attrs:
            attrs = list(filter(lambda a: a[0] == "content", attrs))
            if attrs != []:
                keywords = attrs[0][1]
                self.keywords.extend(keywords.split(","))


parser = KeywordParser()

def parse(html):
    global parser
    parser.feed(html)
    keywords = parser.keywords
    parser = KeywordParser()
    return keywords

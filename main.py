#!/usr/bin/env python3

from flask import Flask, request, jsonify
from Crawler.parser import parse
import Crawler.ipfs as ipfs
from peewee import *
import publishing
import peeweedbevolve
import os, magic, io, json, threading

DATABASE            = os.environ["POSTGRES_DB"]
PASSWORD            = os.environ["POSTGRES_PASSWORD"]
USER                = os.environ["POSTGRES_USER"]
HOST                = os.environ["POSTGRES_HOST"]
changes_count       = 0

database            = PostgresqlDatabase( DATABASE
                                        , password  = PASSWORD
                                        , user      = USER
                                        , host      = HOST
                                        )

ipns_publishing     = threading.Thread(target = publishing.run)
ipns_publishing.start()

http_app            = Flask(__name__)

class BaseModel(Model):
    class Meta():
        database    = database

class CID(BaseModel):
    cid         = TextField(primary_key = True)
    mime        = CharField(max_length = 512)
    status      = CharField()

class Keyword(BaseModel):
    keyword     = TextField(primary_key = True, unique = True)

class KeywordRelation(BaseModel):
    cid         = ForeignKeyField(CID, backref = "keywords")
    keyword     = ForeignKeyField(Keyword, backref = "cids")


def inverted_index():
    keywords        = Keyword.select()
    inverted_index  = { keyword.keyword : [ relation.cid.cid for relation in keyword.cids] for keyword in keywords }
    return inverted_index

def analyse(cid : str):
    file_content    = ipfs.cat(cid)
    assert file_content != False

    if magic.from_buffer(file_content).startswith("HTML document"):
        mimetype        = "text/html"
        keywords        = parse(file_content)
    else:
        mimetype        = "text/plain"
        words           = list(set(file_content.split()))
        words.sort(key = file_content.count)
        keywords        = words[:3]

    cid, _          = CID.get_or_create(cid = cid, mime = mimetype, status = "analyzed")

    for keyword in keywords:
        keyword, _      = Keyword.get_or_create(keyword = keyword)

        KeywordRelation.get_or_create( cid = cid, keyword = keyword)

    return cid

@http_app.before_request
def before_request():
    database.connect()

@http_app.after_request
def after_request(response):
    database.close()
    return response

@http_app.route("/propose")
def propse():
    try:
        global changes_count
        cid = request.values["cid"]
        cid = analyse(cid)
        changes_count += 1
        response    = jsonify(inverted_index())
    except AssertionError:
        return "Invalid CID or Daemon down"
    except KeyError:
        return "Data not provided"

    except Exception as e:
        raise e
    else:
        return response

@http_app.route("/")
def index():
    try:
        response = jsonify(inverted_index())
    except Exception as e:
        raise e
    else:
        return response

def init_database():
    try:
        database.connect()
        database.create_tables([CID, KeywordRelation, Keyword])
        database.close()
    except Exception as e:
        raise e
    else:
        return True

init_database()

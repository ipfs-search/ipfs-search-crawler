# ipfs-search-crawler

MVP of the IPFS Search Crawler.
https://discuss.ipfs.io/t/would-there-be-an-interest-in-an-ipfs-search-engine/8058/10


## The Discovery Process
For now, we don't randomly search.
I don't have a way to get random IPFS files.
Thus you'll have to provide a file CID.
Then the crawler downloads this file and
matches for CID v0 and v1 in this file, downloads 
them and repeats the process.
These CIDs are added to the 
Database with the status "to-be-analyzed"

## The Analysis Process
This process reads a single 
File from the Database, with the status 
"to-be-analyzed". 
It downloads them. analyses them.
This depends upon the detected type of the file.
Using Unix `file` utility.

| MIME Type    | Keyword Analysis                                              |
|--------------|---------------------------------------------------------------|
| `text/plain` | Take the 3 most common words.                                 |
| `text/html`  | Take the meta tags with type keywords string contents as CSV. |

This means, that we are only supporting these two 
MIME types.
After the keywords are analysed, the state of the 
CID is changed from "to-be-analyzed" to 
"analyzed".
If the CID wasn't parsable, the the state would be changed from "to-be-analyzed" to "unanalyzable".

After every 10 scrappes, the database is converted
into a reversed Index and uploaded to the IPNS.
The reversed index is a JSON File like this:

```
{
    "<keyword>" : ["<cid1>", "<cid2">, ...]
}
```
for every keyword.

In order to propose a CID, you just have to enter it here:

```shell
$ curl -X POST http://localhost:4002/propose?cid=<cid>
```


FROM python:3.7

COPY . /crawler
WORKDIR /crawler

RUN pip install -r requirements.txt

EXPOSE 4002
CMD gunicorn -b 0.0.0.0:4002 main:http_app
